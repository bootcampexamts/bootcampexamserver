package com.bootcampexamserver.server.repository;

import com.bootcampexamserver.server.model.FantasyTeamModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FantasyTeamRepository extends CrudRepository<FantasyTeamModel, Long>
{

}
