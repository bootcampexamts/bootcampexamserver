package com.bootcampexamserver.server.controller;

import com.bootcampexamserver.server.model.FantasyTeamModel;
import com.bootcampexamserver.server.repository.FantasyTeamRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
public class FantasyTeamController
{
    // ***** Member variables *****

    private final FantasyTeamRepository team_repo;

    // ***** Constructors *****

    public FantasyTeamController(FantasyTeamRepository team_repo) { this.team_repo = team_repo; }

    // ***** Functions *****

    @GetMapping
    public List<FantasyTeamModel> getItems() { return (List<FantasyTeamModel>) this.team_repo.findAll(); }

    // TODO: FIGURE THIS OUT!
    @PostMapping(value = "/items")
    public void putItems(@RequestBody FantasyTeamModel item) { team_repo.save(item); }

}
