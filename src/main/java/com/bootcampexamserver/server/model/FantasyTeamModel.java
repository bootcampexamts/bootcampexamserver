package com.bootcampexamserver.server.model;


import javax.persistence.*;

@Entity
public class FantasyTeamModel
{
    // ***** Member Variables *****

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long item_id;   // Added for potential case if needing to access database item by id.

    @Column
    private String name;

    @Column
    private int number;

    @Column
    private char position;

    @Column
    private String description;


    // ***** Functions *****

    public long getItem_id()
    {
        return item_id;
    }

    public void setItem_id(long item_id)
    {
        this.item_id = item_id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public int getNumber()
    {
        return number;
    }

    public void setNumber(int number)
    {
        this.number = number;
    }

    public char getPosition()
    {
        return position;
    }

    public void setPosition(char position)
    {
        this.position = position;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }
}
